"use strict";

// Read Existing data from localstorage
const getSavedData = () => {
  const userJSON = localStorage.getItem("userData");
  try {
    return userJSON ? JSON.parse(userJSON) : [];
  } catch (e) {
    return [];
  }
};

// Save User Data on Local Storage
const saveData = (data) => {
  localStorage.setItem("userData", JSON.stringify(data));
};

// Remove User Data
const removeUser = (savedData, userId) => {
  const userIndex = savedData.findIndex((obj) => obj.id === userId);
  if (userIndex > -1) {
    savedData.splice(userIndex, 1);
  }
};

// Verify email and Password
const verifyCred = (savedData, userId) => {
  const userObject = savedData.find((obj) => obj.id === userId);
  if (userObject === undefined) {
    return false;
  } else {
    return true;
  }
};

// Get Credentials
const getCred = (savedData, userId) => {
  const userObject = savedData.find((obj) => obj.id === userId);
  if (userObject === undefined) {
    return {};
  } else {
    return userObject;
  }
};

// Get Credentials Index from saved Array
const getIndex = (savedData, userId) =>
  savedData.findIndex((obj) => obj.id === userId);

// Open Edit Modal
const openEditModal = (e) => {
  document.querySelector("#editModal").style.display = "block";
};

// Close Edit Modal
const closeEditModal = (e) => {
  document.querySelector("#editModal").style.display = "none";
  location.assign(`./user.html#${userHashId}`);
};

// Open Delete Modal
const openDeleteModal = (e) => {
  document.querySelector("#deleteModal").style.display = "block";
  location.assign(`./user.html#${userHashId}`);
};

// Close Delete Modal
const closeDeleteModal = (e) => {
  document.querySelector("#deleteModal").style.display = "none";
  location.assign(`./user.html#${userHashId}`);
};
