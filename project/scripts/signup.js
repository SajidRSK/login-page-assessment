"use strict";
let emailInput,
  passwordInput,
  dobInput,
  savedData = getSavedData(),
  userData = {
    id: "",
    name: "",
    email: "",
    password: "",
    dob: "",
    position: "",
  };

document.querySelector("#email").addEventListener("input", (e) => {
  userData.email = e.target.value;
  emailInput = e.target.value;
});

document.querySelector("#password").addEventListener("input", (e) => {
  userData.password = e.target.value;
  passwordInput = e.target.value;
});
document.querySelector("#position").addEventListener("change", (e) => {
  userData.position = e.target.value;
});

document.querySelector("#birthday").addEventListener("change", (e) => {
  userData.dob = e.target.value;
  dobInput = e.target.value;
});

// Sign Up
document.querySelector("#formSubmit").addEventListener("submit", (e) => {
  e.preventDefault();
  const userId = emailInput.split("@")[0];
  const isUserAvailable = verifyCred(savedData, userId);
  const getSavedCred = getCred(savedData, userId);
  const doesIdExist = getSavedCred.id === userId;
  if (passwordInput.length < 8) {
    alert("Password Must be more than 8 characters");
  } else if (moment().diff(dobInput, "years") < 15) {
    alert("You Must Be Over 15 to visit this site !");
  } else {
    if (isUserAvailable || doesIdExist) {
      alert("This email address is already registered. Please Login");
    } else {
      userData.id = userData.email.split("@")[0];
      savedData.push(userData);
      saveData(savedData);
      location.assign(`./user.html#${userId}`);
    }
  }
});
