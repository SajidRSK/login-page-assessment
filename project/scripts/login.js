"use strict";
let savedData = getSavedData(),
  emailInput,
  passwordInput;

document.querySelector("#email").addEventListener("input", (e) => {
  emailInput = e.target.value;
});

document.querySelector("#password").addEventListener("input", (e) => {
  passwordInput = e.target.value;
});

// Log In
document.querySelector("#loginForm").addEventListener("submit", (e) => {
  e.preventDefault();
  const userId = emailInput.split("@")[0];
  const isUserAvailable = verifyCred(savedData, userId);
  const getSavedCred = getCred(savedData, userId);
  if (!isUserAvailable) {
    alert("email address is not registered! Please Sign Up");
  } else {
    if (
      emailInput === getSavedCred.email &&
      passwordInput === getSavedCred.password
    ) {
      location.assign(`./user.html#${userId}`);
    } else {
      alert("Enter Correct password");
    }
  }
});
