"use strict";
const userHashId = location.hash.substring(1);
let savedData = getSavedData();
let userIndex = getIndex(savedData, userHashId);
const getSavedCred = getCred(savedData, userHashId);
let newHashId = userHashId,
  emailInput,
  passInput,
  nameInput;
// Append User Name
const name = document.createElement("p");
name.textContent = getSavedCred.name;
if (name.textContent.length < 1) {
  name.textContent = "Edit Your Name Please";
}
document.querySelector("#userName").appendChild(name);

// Append User Email
const mail = document.createElement("p");
mail.textContent = getSavedCred.email;
document.querySelector("#userEmail").appendChild(mail);

// Append User DOB
const dob = document.createElement("p");
dob.textContent = getSavedCred.dob;
document.querySelector("#userDob").appendChild(dob);

// Append User Position
const position = document.createElement("p");
position.textContent = getSavedCred.position;
position.textContent =
  position.textContent.charAt(0).toUpperCase() + position.textContent.slice(1);
document.querySelector("#userPosition").appendChild(position);

// Open Edit Modal
document.querySelector("#editButton").addEventListener("click", openEditModal);

// Take Inputs From Edit Modal
document.querySelector("#modalName").addEventListener("change", (e) => {
  nameInput = e.target.value;
  // savedData[userIndex].name = e.target.value;
});

document.querySelector("#modalEmail").addEventListener("change", (e) => {
  emailInput = e.target.value;
});

document.querySelector("#modalPassword").addEventListener("change", (e) => {
  passInput = e.target.value;
  // savedData[userIndex].password = e.target.value;
});
document.querySelector("#modalPosition").addEventListener("change", (e) => {
  savedData[userIndex].position = e.target.value;
});

document.querySelector("#modalDob").addEventListener("change", (e) => {
  savedData[userIndex].dob = e.target.value;
});

// If no id exist go to login page
const pageAvailable = savedData.find((obj) => obj.id === userHashId);
if (pageAvailable === undefined) {
  location.assign("./index.html");
}

// Editing User Data using Modal
document.querySelector("#modalFormOk").addEventListener("click", (e) => {
  if (savedData[userIndex].password.length < 8) {
    alert("Password Must be more than 8 characters");
    location.assign(`./user.html#${userHashId}`);
  } else if (moment().diff(savedData[userIndex].dob, "years") < 15) {
    alert("You Must Be Over 15 to visit this site !");
    location.assign(`./user.html#${userHashId}`);
  } else {
    // if user changes the email address
    if (emailInput === undefined) emailInput = "";
    else {
      if (emailInput.length > 0) {
        savedData[userIndex].email = emailInput;
        newHashId = emailInput.split("@")[0];
        savedData[userIndex].id = emailInput.split("@")[0];
      }
    }
    // if user changes the Password
    if (passInput === undefined) passInput = "";
    else {
      if (passInput.length > 0) savedData[userIndex].password = passInput;
    }
    // if user changes the Name
    if (nameInput === undefined) nameInput = "";
    else {
      if (nameInput.length > 0) savedData[userIndex].name = nameInput;
    }

    // Edit User Profile
    e.preventDefault();
    saveData(savedData);
    closeEditModal();
    location.assign(`./user.html#${newHashId}`);
    location.reload();
  }
});

// Close Edit Modal
document
  .querySelector(".closeFormBtn")
  .addEventListener("click", closeEditModal);

// Cancel Edit Modal
document
  .querySelector("#modalFormCancel")
  .addEventListener("click", closeEditModal);

// Open Confirmation Modal
document
  .querySelector("#deleteButton")
  .addEventListener("click", openDeleteModal);

// Close Confirmation Modal
document
  .querySelector("#modalDeleteCancel")
  .addEventListener("click", closeDeleteModal);

// Delete Data
document.querySelector("#modalDeleteOk").addEventListener("click", (e) => {
  savedData.splice(userIndex, 1);
  location.assign(`./index.html`);
  saveData(savedData);
});
